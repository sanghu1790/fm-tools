#!/bin/bash

# Enable exit on error
#set -e
set -x

echo "Calculating files that were changed by the MR."

# Debug output
echo "CI_MERGE_REQUEST_TARGET_BRANCH_NAME: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
echo "CI_MERGE_REQUEST_SOURCE_BRANCH_NAME: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
echo "CI_MERGE_REQUEST_SOURCE_PROJECT_PATH: $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH"
echo "CI_PROJECT_NAME: $CI_PROJECT_NAME"
echo "CI_PROJECT_PATH: $CI_PROJECT_PATH"
echo "CI_PROJECT_NAMESPACE: $CI_PROJECT_NAMESPACE"
echo "CI_PROJECT_URL: $CI_PROJECT_URL"

echo "Current branch: $(git branch --show-current)"

if [[ "$CI_PROJECT_PATH" =~ ^"sosy-lab" ]]; then
    # Source is SoSy-Lab
    echo "Project path space: $CI_PROJECT_PATH"
    SOURCE_REPO="origin"
    SOURCE_BRANCH_PREFIX="$SOURCE_REPO/"
    SOURCE_BRANCH="$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
    TARGET_REPO="origin"
    TARGET_BRANCH_PREFIX="$TARGET_REPO/"
    TARGET_BRANCH="$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
elif [ -z "$CI_PROJECT_PATH" ]; then
    # Source is Local
    SOURCE_REPO="origin"
    SOURCE_BRANCH_PREFIX=""
    SOURCE_BRANCH="$(git branch --show-current)"
    TARGET_REPO="origin"
    TARGET_BRANCH_PREFIX="origin/"
    TARGET_BRANCH="main"
else
    # Source is fork of GitLab user
    echo "Project path space: $CI_PROJECT_PATH"
    SOURCE_REPO="origin"
    SOURCE_BRANCH_PREFIX="$SOURCE_REPO/"
    SOURCE_BRANCH="$(git branch --show-current)"
    TARGET_REPO="upstream"
    TARGET_BRANCH_PREFIX="$TARGET_REPO/"
    TARGET_BRANCH="main"
fi

git fetch --depth=100 "$SOURCE_REPO" "$SOURCE_BRANCH"
git fetch --depth=100 "$TARGET_REPO" "$TARGET_BRANCH"

MERGE_BASE_COMMIT=$(git merge-base "${SOURCE_BRANCH_PREFIX}${SOURCE_BRANCH}" "${TARGET_BRANCH_PREFIX}${TARGET_BRANCH}")
echo "Merge-base commit:"
echo "$MERGE_BASE_COMMIT"
CHANGED_YML_FILES=$(git diff --name-only --diff-filter=d "${MERGE_BASE_COMMIT}..${SOURCE_BRANCH_PREFIX}${SOURCE_BRANCH}" -- data/*.yml)
echo "Changed YML files:"
echo "$CHANGED_YML_FILES"
