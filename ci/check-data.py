#!/usr/bin/env python3
import argparse
import logging
import jsonschema
from pathlib import Path
import json
import yaml
import sys
from collections import Counter
import _util as util
import xml.etree.ElementTree as ET
from urllib.request import urlopen, Request, HTTPError
from os.path import basename


def _load_schema(path: Path):
    with open(path, "r") as schema:
        return json.load(schema)


def _load_yaml(path: Path):
    with open(path, "r") as file:
        return yaml.safe_load(file)


def validate_no_repetitions_in_techniques(yaml_data, tool_name):
    count = Counter(yaml_data["techniques"])
    success = True
    for k, v in count.items():
        if v > 1:
            util.error(f"{tool_name}: Repeated entry {k} in techniques.")
            success = False
    return success


def _request_benchmark_def(benchmark_url):
    try:
        r = Request(benchmark_url, headers={"User-Agent": "Mozilla/5.0"})
        request = urlopen(r)
        urlAvailable = request.getcode() == 200
        return (urlAvailable, request)
    except HTTPError:
        return (False, None)


def compare_to_benchdef(yaml_data, tool_name: str, year: int):
    participates = False
    for participation in yaml_data["competition_participations"]:
        competition_year = int(participation["competition"].split(" ")[1])
        if competition_year != year:
            continue
        participates = True
        competition = participation["competition"].split(" ")[0]
        xml = f"https://gitlab.com/sosy-lab/{competition.lower()}/bench-defs/-/raw/main/benchmark-defs/{basename(tool_name)[:-len('.yml')]}.xml"
        exists, request = _request_benchmark_def(xml)
        if exists:
            content = request.read().decode("utf-8")
            benchmark_definition = ET.fromstring(content)
            options = []
            for option in benchmark_definition.findall("option"):
                # get name of option
                options.append(option.get("name"))
                if option.text:
                    options.append(option.text)
            if participation["competition"] == f"{competition} {year}":
                result = True
                for version in yaml_data["versions"]:
                    if version["version"] == participation["tool_version"]:
                        if version["benchexec_toolinfo_options"] != options:
                            util.error(
                                f"{tool_name}: Comparing bench-def options {options} with fm-tool options {version['benchexec_toolinfo_options']} for version '{version['version']}' in track '{participation['track']}' failed."
                            )
                            result = False
                if not result:
                    return False
        else:
            util.error(f"{tool_name}: No benchmark definition found at {xml}.")
            return False
    if participates:
        util.info(f"{tool_name}: All options for {year} match.")
    else:
        util.info(f"{tool_name}: Does not participate in {year}.")
    return True


def check_participation(yaml_data, tool: str, year: int):
    participations = yaml_data["competition_participations"]
    for participation in participations:
        if participation["competition"].endswith(f"{year}"):
            competition = participation["competition"].split(" ")[0]
            for version in yaml_data["versions"]:
                if version["version"] == participation["tool_version"]:
                    util.info(
                        f"{tool}: Found {participation['tool_version']} for {competition} {year}"
                    )
                    return True
            util.error(
                f"{tool}: The tool listed a participation for {competition} {year} but the version {participation['tool_version']} is not listed in the versions."
            )
            return False
    util.info(f"{tool} does not participate in {year}")
    return True


def validate_property_order(yaml_data, tool_path: Path, schema: dict) -> bool:
    expected_order = [
        "name",
        "input_languages",
        "project_url",
        "repository_url",
        "spdx_license_identifier",
        "coveriteam_actor",
        "benchexec_toolinfo_module",
        "fmtools_format_version",
        "fmtools_entry_maintainers",
        "maintainers",
        "versions",
        "competition_participations",
        "techniques",
    ]

    all_expected_keys = set(expected_order)
    all_schema_keys = set(schema["properties"].keys())
    all_schema_keys.remove("literature")

    if all_schema_keys != all_expected_keys:
        util.error(
            f"{tool_path}: Schema contains more keys than listed in expected order: {all_schema_keys.difference(all_expected_keys)} OR {all_expected_keys.difference(all_schema_keys)}."
        )

    yaml_keys = list(yaml_data.keys())

    if yaml_keys[: len(expected_order)] != expected_order:
        missing_properties = [prop for prop in expected_order if prop not in yaml_keys]
        extra_properties = [prop for prop in yaml_keys if prop not in expected_order]

        if missing_properties:
            util.error(f"{tool_path}: Properties {missing_properties} missing.")
        if extra_properties:
            util.error(f"{tool_path}: Properties {extra_properties} must be removed.")
        if not missing_properties and not extra_properties:
            util.error(
                f"{tool_path}: Properties must follow the given order {expected_order}"
            )

        return False
    return True


def main(args):
    try:
        schema = _load_schema(args.schema)
        yaml_data = _load_yaml(args.fm_data)
        status = validate_property_order(yaml_data, args.fm_data, schema)
        jsonschema.validate(yaml_data, schema)
        status &= validate_no_repetitions_in_techniques(yaml_data, args.fm_data)
        status &= check_participation(yaml_data, args.fm_data, args.year)
        status &= compare_to_benchdef(yaml_data, args.fm_data, args.year)
        if status:
            util.info(f"{args.fm_data} matches the schema.")
            sys.exit(0)
        sys.exit(1)
    except jsonschema.exceptions.ValidationError as e:
        util.error(f"{args.fm_data} does not match the schema. {e=}")
    except jsonschema.exceptions.SchemaError as e:
        util.error(
            f"The provided schema is wrong. Please notify one of the maintainers of the repository. {e=}"
        )
    sys.exit(1)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format=None)
    parser = argparse.ArgumentParser(description="Validate YAML definitions of tools.")
    parser.add_argument(
        "--schema",
        type=Path,
        default=Path("fm-tools/data/schema.json"),
        required=True,
        help="Path to the JSON schema file",
    )
    parser.add_argument(
        "--fm-data", type=Path, required=True, help="Path to the YAML tool file"
    )
    parser.add_argument(
        "--year", type=int, required=True, help="Competition year (e.g., 2024)"
    )
    args = parser.parse_args()
    main(args)
